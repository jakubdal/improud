package transport

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/jakubdal/improud/unpack"
)

type pridemanager interface {
	PrideCommand(
		ctx context.Context,
		command unpack.PrideCommand,
	) error
}

// Init initializes HTTP routing
func Init(manager pridemanager) http.Handler {
	r := chi.NewRouter()

	r.Post("/command", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Printf("read command body: %v\n", err)
			return
		}

		command, err := unpack.Command(r.Context(), b)
		if err != nil {
			fmt.Printf("unpack.Command: %v\n", err)
			return
		}

		if err := manager.PrideCommand(r.Context(), command); err != nil {
			fmt.Printf("manager.PrideCommand: %v", err)
			return
		}
	}))

	return r
}
