package main

// Config contains applications's configuration
type Config struct {
	Listen   string `env:"LISTEN" envDefault:":8080"`
	Location string `env:"LOCATION" envDefault:"CET"`

	SlackToken string `env:"SLACK_TOKEN" envDefault:""`

	GKE              bool   `env:"GKE" envDefault:"false"`
	PostgresHost     string `env:"POSTGRES_HOST" envDefault:"localhost"`
	PostgresPort     string `env:"POSTGRES_PORT" envDefault:"5432"`
	PostgresDatabase string `env:"POSTGRES_DATABASE" envDefault:"improud"`
	PostgresUser     string `env:"POSTGRES_USER" envDefault:"postgres"`
	PostgresPassword string `env:"POSTGRES_PASSWORD" envDefault:"postgres"`
}
