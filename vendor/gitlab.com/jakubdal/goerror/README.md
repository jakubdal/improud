# Goerror

Goerror is Golang utility library for error handling. It makes heavy use of [github.com/pkg/errors](github.com/pkg/errors).

## Installation

Run `go get -u gitlab.com/jakubdal/goerror` to import `goerror` to your project.

## Usage

Let's say, you have a following function:

```
func DoSomething() error
```

Usually a lot can go wrong inside the `DoSomething` function, and you will not be able to tell, what's the reason. Unless...

You can use `goerror.WithHTTPStatusCode(err, http.StatusBadRequest)` to signal, that the error was caused by invalid request, or `goerror.WithHTTPStatusCode(err, http.Forbidden)` to contain the information, that user does not have enough permissions.

Then you can also write a message, that won't be put into logs, but will be returned to a user with the help of `goerror.WithUserMessage(err, "INVALID_PASSWORD")`. So while you as a programmer may not care if it's `INVALID_PASSWORD`, or `INVALID_EMAIL`, this will allow you, to return "const-coded" messages, or just plain user messages, to let them know what went wrong.

To then use the full power, in your http handler, you can just use `http.Error(w, goerror.UserMessage(err), goerror.HTTPStatusCode(err))` to return the appropriate error :)
