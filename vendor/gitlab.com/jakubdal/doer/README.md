# ![Gopher Eyeroll](https://github.com/egonelbre/gophers/blob/master/icon/eyeroll.gif?raw=true) HTTPDoer

I've copy-pasted this interface way too many times. So I've made a repository for it, because I don't care about dependencies.

## Setup

`go get -u gitlab.com/jakubdal/doer`

Then use it in your package with

```
import (
  "gitlab.com/jakubdal/doer"
)

// SomeFunction can now mock HTTP requests and use custom HTTP client
func SomeFunction(doer doer.HTTPDoer) {
  ...
}
```

## Mocking

In order to mock a doer, you can use `doer.MockHTTPDoer` as follows:

```
import (
  "gitlab.com/jakubdal/doer"
)

func(doer doer.HTTPDoer) {
  ...
}(doer.MockHTTPDoer{
  DoFunc func(req *http.Request) (*http.Response, error) {
    return nil, fmt.Errorf("doer error")
  },
})
```

## Credits

[Let the Doer Do it](https://www.0value.com/let-the-doer-do-it) blog post by [0value.com](https://www.0value.com/) - for the inspiration to use the mighty HTTPDoer everywhere I go.

[Gophers](https://github.com/egonelbre/gophers) by [egonelbre](https://github.com/egonelbre) - for the aweesome eyeroll icon used in the title
