package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/caarlos0/env"
	"github.com/jmoiron/sqlx"
	"github.com/nlopes/slack"
	"gitlab.com/jakubdal/improud/formating"
	"gitlab.com/jakubdal/improud/pride"
	"gitlab.com/jakubdal/improud/transport"
	// Cloud postgres
	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/postgres"
	// PostgreSQL driver
	_ "github.com/lib/pq"
)

func main() {
	conf := Config{}
	env.Parse(&conf)

	var err error
	var db *sqlx.DB
	if conf.GKE {
		db, err = sqlx.Open(
			"cloudsqlpostgres",
			fmt.Sprintf(
				"host=%s dbname=%s user=%s password=%s sslmode=disable",
				conf.PostgresHost, conf.PostgresDatabase, conf.PostgresUser,
				conf.PostgresPassword,
			))
	} else {
		db, err = sqlx.Connect(
			"postgres",
			fmt.Sprintf(
				"host=%s port=%s dbname=%s user=%s password=%s",
				conf.PostgresHost, conf.PostgresPort, conf.PostgresDatabase,
				conf.PostgresUser, conf.PostgresPassword,
			),
		)
	}
	if err != nil {
		log.Fatalf("sqlx.Open/Connect: %v", err)
	}
	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(5)
	db.SetConnMaxLifetime(time.Minute)

	slackClient := slack.New(conf.SlackToken)
	users, err := slackClient.GetUsers()
	if err != nil {
		log.Fatalf("slackClient.GetUsers: %v", err)
	}
	for _, user := range users {
		if user.Deleted || user.IsBot {
			continue
		}
		var usersExisting int
		if err := db.GetContext(
			context.Background(),
			&usersExisting,
			`SELECT
				COUNT(*)
			FROM
				slack_user
			WHERE
				id = $1;
			`, user.ID); err != nil {
			log.Fatalf("db.GetContext (existing user): %v", err)
		}
		if usersExisting > 0 {
			continue
		}
		if _, err := db.ExecContext(
			context.Background(),
			`INSERT INTO
				slack_user(id, username, fullname)
			VALUES
				($1, $2, $3);`,
			user.ID, formating.NormalizeUsername(user.Name), user.RealName,
		); err != nil {
			log.Fatalf("db.ExecContext: %v", err)
		}
	}

	location, err := time.LoadLocation(conf.Location)
	if err != nil {
		fmt.Printf(
			"failed loading location %v, fallback to %v\n",
			conf.Location, time.Local.String(),
		)
		location = time.Local
	}
	fmt.Println("Server started on", conf.Listen)
	log.Fatalf(
		"http.ListenAndServe: %v\n",
		http.ListenAndServe(conf.Listen, transport.Init(
			pride.NewManager(
				pride.NewStorage(db, location),
				pride.NewMessenger(http.DefaultClient),
				db,
				location,
			),
		)),
	)
}
