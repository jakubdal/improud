package errtype

const (
	// UserNotFound is used when provided user was not found
	UserNotFound string = "User not found."
	// NoSelfPromotion is used when user tries to give pride to himself
	NoSelfPromotion string = "Bragging is not allowed."
)
