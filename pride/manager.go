package pride

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/jakubdal/goerror"
	"gitlab.com/jakubdal/improud/errtype"
	"gitlab.com/jakubdal/improud/unpack"
)

// Change contains all informations about change to pride
type Change struct {
	Giver    User
	Receiver User
	Amount   int
}

type storage interface {
	Change(ctx context.Context, change Change) error
	Get(ctx context.Context, timestampBegin *time.Time, timestampEnd *time.Time) ([]User, error)
}

type messenger interface {
	PrideChange(responseURL string, change Change) error
	PrideLeaderboard(responseURL string, users []User, monthly bool) error
	Error(responseURL string, signaledErr error) error
}

// Manager manages everything about related to pride
type Manager struct {
	s   storage
	m   messenger
	db  *sqlx.DB
	loc *time.Location
}

// NewManager creates new Manager isntance
func NewManager(
	s storage,
	m messenger,
	db *sqlx.DB,
	loc *time.Location,
) *Manager {
	return &Manager{s: s, m: m, db: db, loc: loc}
}

// PrideCommand handles unpack.PrideCommand regarding pride commadns
func (m *Manager) PrideCommand(
	ctx context.Context,
	command unpack.PrideCommand,
) error {
	var err error
	switch command.Command {
	case "improud":
		err = errors.Wrap(
			m.commandImProud(ctx, command),
			"commandImProud",
		)
	case "pridemonth":
		err = errors.Wrap(
			m.commandPrideMonth(ctx, command),
			"commandPrideMonth",
		)
	case "pridetotal":
		err = errors.Wrap(
			m.commandPrideTotal(ctx, command),
			"commandPrideTotal",
		)
	}

	if err != nil {
		if msgErr := m.m.Error(command.ResponseURL, err); msgErr != nil {
			fmt.Printf("error signaling error: %v\n", msgErr)
		}
	}

	return err
}

func (m *Manager) commandPrideMonth(
	ctx context.Context,
	command unpack.PrideCommand,
) error {
	now := time.Now().In(m.loc)
	timestampBegin := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 1, m.loc)
	nextMonthYear, nextMonthMonth := now.Year(), now.Month()
	if nextMonthMonth == time.December {
		nextMonthYear++
		nextMonthMonth = time.January
	} else {
		nextMonthMonth++
	}
	timestampEnd := time.Date(nextMonthYear, nextMonthMonth, 1, 0, 0, 0, 1, m.loc)

	users, err := m.s.Get(ctx, &timestampBegin, &timestampEnd)
	if err != nil {
		return errors.Wrap(err, "storage.Get")
	}
	return errors.Wrap(
		m.m.PrideLeaderboard(command.ResponseURL, users, true),
		"messenger.PrideLeadeboard (month)",
	)
}

func (m *Manager) commandPrideTotal(
	ctx context.Context,
	command unpack.PrideCommand,
) error {
	users, err := m.s.Get(ctx, nil, nil)
	if err != nil {
		return errors.Wrap(err, "storage.Get")
	}
	return errors.Wrap(
		m.m.PrideLeaderboard(command.ResponseURL, users, false),
		"messenger.PrideLeadeboard (month)",
	)
}

func (m *Manager) commandImProud(
	ctx context.Context,
	command unpack.PrideCommand,
) error {
	var receiver User
	if err := m.db.GetContext(
		ctx,
		&receiver,
		`SELECT
			id, username, fullname
		FROM
			slack_user
		WHERE
			username = $1`, command.ReceiverName); err != nil {
		return goerror.WithUserMessage(
			errors.Wrap(err, "get receiver"),
			errtype.UserNotFound,
		)
	}
	var giver User
	if err := m.db.GetContext(
		ctx,
		&giver,
		`SELECT
			id, username, fullname
		FROM
			slack_user
		WHERE
			id = $1`, command.GiverID); err != nil {
		// TODO - NAMED ERROR?
		return errors.Wrap(err, "get giver")
	}
	if receiver.ID == giver.ID {
		return goerror.WithUserMessage(
			fmt.Errorf("bragging try from: %v", giver.ID),
			errtype.NoSelfPromotion,
		)
	}

	change := Change{
		Receiver: receiver,
		Giver:    giver,
		Amount:   command.Amount,
	}

	if err := m.s.Change(ctx, change); err != nil {
		return errors.Wrap(err, "storage.Change")
	}

	if err := m.m.PrideChange(command.ResponseURL, change); err != nil {
		return errors.Wrap(err, "messenger.PrideChange")
	}

	return nil
}
