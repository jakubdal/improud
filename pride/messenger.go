package pride

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/pkg/errors"
	"gitlab.com/jakubdal/doer"
	"gitlab.com/jakubdal/goerror"
)

// ResponseType is type of response sent to Slack
type ResponseType string

// slackAttachment is an attachment added to Slack message
type slackAttachment struct {
	Fallback string                 `json:"fallback"`
	Pretext  string                 `json:"pretext"`
	Title    string                 `json:"title"`
	Text     string                 `json:"text"`
	Fields   []slackAttachmentField `json:"fields"`
}

// slackAttachmentField is a field that's inside slack attachment
type slackAttachmentField struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}

const (
	// ResponseTypeEphemeral shows slash command and message only
	// to user, who sent it
	ResponseTypeEphemeral ResponseType = "ephemeral"
	// ResponseTypeChannel shows both the command and message in the
	// channel
	ResponseTypeChannel ResponseType = "in_channel"
)

// Messenger is responsible for sending messages to slack
// with informations about pride
type Messenger struct {
	doer doer.HTTPDoer
}

// slackResponse is content that we want to send to slack
// in response to an event
type slackResponse struct {
	Type        ResponseType      `json:"response_type"`
	Text        string            `json:"text"`
	Attachments []slackAttachment `json:"attachments"`
}

// NewMessenger creates new instance of Messenger
func NewMessenger(doer doer.HTTPDoer) *Messenger {
	return &Messenger{doer: doer}
}

// Error sends an error message to user
func (m *Messenger) Error(
	responseURL string,
	signaledErr error,
) error {
	userText := goerror.UserMessage(signaledErr)
	if userText == "" {
		userText = "Something went wrong, no pride has been changed."
	}

	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(slackResponse{
		Type: ResponseTypeEphemeral,
		Text: userText,
	}); err != nil {
		return errors.Wrap(err, "encode payload")
	}
	req, err := http.NewRequest(
		http.MethodPost,
		responseURL,
		&buf,
	)
	if err != nil {
		return errors.Wrap(err, "create request")
	}
	resp, err := m.doer.Do(req)
	if resp != nil && resp.Body != nil {
		defer func() {
			io.Copy(ioutil.Discard, resp.Body)
			resp.Body.Close()
		}()
	}
	if err != nil {
		return errors.Wrap(err, "do request")
	}
	return nil
}

// PrideLeaderboard sends the information about pride leaderboard
func (m *Messenger) PrideLeaderboard(
	responseURL string,
	users []User,
	monthly bool,
) error {
	var buf bytes.Buffer

	text := "This month we're most proud of..."
	if !monthly {
		text = "In total, we're most proud of..."
	}

	attachment := slackAttachment{
		Title:  "Leaderboard",
		Fields: make([]slackAttachmentField, 0, len(users)),
	}
	for i, user := range users {
		usernameText := ""
		prideText := ""
		if i == 0 {
			usernameText = "Username"
			prideText = "Pride"
		}
		attachment.Fields = append(
			attachment.Fields,
			slackAttachmentField{
				Title: usernameText,
				Value: user.Name(),
				Short: true,
			},
			slackAttachmentField{
				Title: prideText,
				Value: strconv.Itoa(user.Pride),
				Short: true,
			},
		)
	}

	if err := json.NewEncoder(&buf).Encode(slackResponse{
		Type:        ResponseTypeChannel,
		Text:        text,
		Attachments: []slackAttachment{attachment},
	}); err != nil {
		return errors.Wrap(err, "encode payload")
	}
	req, err := http.NewRequest(
		http.MethodPost,
		responseURL,
		&buf,
	)
	if err != nil {
		return errors.Wrap(err, "create request")
	}
	resp, err := m.doer.Do(req)
	if resp != nil && resp.Body != nil {
		defer func() {
			io.Copy(ioutil.Discard, resp.Body)
			resp.Body.Close()
		}()
	}
	if err != nil {
		return errors.Wrap(err, "do request")
	}
	return nil
}

// PrideChange is information about pride change
func (m *Messenger) PrideChange(
	responseURL string,
	change Change,
) error {
	var buf bytes.Buffer

	changeText := "increased"
	if change.Amount < 0 {
		changeText = "decreased"
	}
	if err := json.NewEncoder(&buf).Encode(slackResponse{
		Type: ResponseTypeChannel,
		Text: fmt.Sprintf(
			"%v has %v %v's pride by %v!",
			change.Giver.Name(),
			changeText,
			change.Receiver.Name(),
			abs(change.Amount),
		),
	}); err != nil {
		return errors.Wrap(err, "encode payload")
	}
	req, err := http.NewRequest(
		http.MethodPost,
		responseURL,
		&buf,
	)
	if err != nil {
		return errors.Wrap(err, "create request")
	}
	resp, err := m.doer.Do(req)
	if resp != nil && resp.Body != nil {
		defer func() {
			io.Copy(ioutil.Discard, resp.Body)
			resp.Body.Close()
		}()
	}
	if err != nil {
		return errors.Wrap(err, "do request")
	}
	return nil
}
