package pride

import (
	"context"
	"database/sql"
	"fmt"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/jakubdal/goerror"
)

const (
	dailyPrideLimit = 5
)

// Storage is used to store and fetch pride
type Storage struct {
	db  *sqlx.DB
	loc *time.Location

	mutex sync.RWMutex
}

// NewStorage returns new instance of Storage
func NewStorage(db *sqlx.DB, loc *time.Location) *Storage {
	return &Storage{db: db, loc: loc}
}

// Change marks a change to `receiverName` pride by `giverID`
func (s *Storage) Change(
	ctx context.Context,
	change Change,
) error {
	if change.Amount == 0 {
		return nil
	}

	now := time.Now().In(s.loc)
	todayStart := time.Date(
		now.Year(), now.Month(), now.Day(), 0, 0, 0, 1, s.loc,
	)
	tomorrowStart := todayStart.Add(time.Hour * 24)
	s.mutex.Lock()
	defer s.mutex.Unlock()

	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		return errors.Wrap(err, "s.db.BeginTxx")
	}
	defer tx.Rollback()

	amountCheck := "amount > 0"
	if change.Amount < 0 {
		amountCheck = "amount < 0"
	}
	var sqlPride sql.NullInt64
	if err := tx.GetContext(
		ctx,
		&sqlPride,
		fmt.Sprintf(`SELECT
      SUM(amount)
    FROM
      pride_given
    WHERE
        deleted = FALSE
      AND
        given_by = $1
      AND
        (given_at BETWEEN $2 AND $3)
      AND
        %v;`, amountCheck),
		change.Giver.ID, todayStart, tomorrowStart,
	); err != nil {
		return errors.Wrap(err, "get given pride")
	}
	givenPride := 0
	if sqlPride.Valid {
		givenPride = int(sqlPride.Int64)
	}

	if abs(givenPride+change.Amount) > dailyPrideLimit {
		prideType := "positive"
		if change.Amount < 0 {
			prideType = "negative"
		}
		return goerror.WithUserMessage(
			fmt.Errorf("pride limit reached"),
			fmt.Sprintf(
				"You've given enough %v pride for today (current count: %v).",
				prideType, givenPride,
			),
		)
	}

	if _, err := tx.ExecContext(
		ctx,
		`INSERT INTO
      pride_given(given_by, given_to, amount, given_at)
    VALUES
      ($1, $2, $3, $4);`,
		change.Giver.ID, change.Receiver.ID, change.Amount, now,
	); err != nil {
		return errors.Wrap(err, "tx.ExecContext")
	}

	if err := tx.Commit(); err != nil {
		return errors.Wrap(err, "tx.Commit")
	}

	return nil
}

// Get gets pride that team has of given user witin given timeframe
func (s *Storage) Get(
	ctx context.Context,
	timestampBegin *time.Time,
	timestampEnd *time.Time,
) ([]User, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	timeQuery := ""
	if timestampBegin != nil && timestampEnd != nil {
		timeQuery = "WHERE (given_at BETWEEN $1 AND $2)"
	}
	query := fmt.Sprintf(`SELECT
		slack_user.id,
		username,
		fullname,
		pride
	FROM
			slack_user
		JOIN
			(
				SELECT
					given_to,
					SUM(amount) AS pride
				FROM
					pride_given %v
				GROUP BY
					given_to
				ORDER BY
					pride DESC
			) AS total
		ON
			slack_user.id = total.given_to;`, timeQuery)

	var users []User
	if timestampBegin != nil && timestampEnd != nil {
		if err := s.db.SelectContext(
			ctx,
			&users,
			query,
			*timestampBegin, *timestampEnd,
		); err != nil {
			return users, errors.Wrap(err, "s.db.GetContext")
		}
	} else {
		if err := s.db.SelectContext(
			ctx,
			&users,
			query,
		); err != nil {
			return users, errors.Wrap(err, "s.db.GetContext")
		}
	}

	return users, nil
}

// abs returns absolute value of given integer
func abs(i int) int {
	if i > 0 {
		return i
	}
	return -i
}
