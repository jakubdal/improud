package pride

import "strings"

// User contains informations about user
type User struct {
	ID       string `db:"id"`
	UserName string `db:"username"`
	FullName string `db:"fullname"`
	Pride    int    `db:"pride"`
}

// Name returns FullName, or UserName if FullName is empty
func (u User) Name() string {
	if strings.TrimSpace(u.FullName) == "" {
		return strings.TrimSpace(u.UserName)
	}
	return strings.TrimSpace(u.FullName)
}
