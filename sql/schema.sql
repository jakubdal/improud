DROP TABLE pride_given;
DROP TABLE slack_user;

CREATE TABLE slack_user (
  id CHAR(10) PRIMARY KEY,
  username TEXT,
  fullname TEXT
);

CREATE TABLE pride_given (
  id SERIAL,
  given_by CHAR(10) REFERENCES slack_user(id),
  given_to CHAR(10) REFERENCES slack_user(id),
  amount INT,
  given_at TIMESTAMP,

  deleted BOOLEAN DEFAULT FALSE
);
