package formating

import "strings"

// NormalizeUsername takes and form of username and
// returns it in normalized form
func NormalizeUsername(in string) string {
	return strings.ToLower(strings.Replace(in, "@", "", -1))
}
