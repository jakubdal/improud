package unpack

import (
	"context"
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/jakubdal/improud/formating"
)

// PrideCommand contains informations pulled from pride request
type PrideCommand struct {
	Token   string
	Command string

	GiverID      string
	GiverName    string
	ReceiverName string
	Amount       int
	ResponseURL  string
}

// Command unpacks command request from Slack and returns
// value of pride and user to which the pride should be
// added/removed from
//
// This function does not check if user with provided username
// exists
func Command(
	ctx context.Context,
	rawCommand []byte,
) (PrideCommand, error) {
	var command PrideCommand
	values, err := url.ParseQuery(string(rawCommand))
	if err != nil {
		return command, errors.Wrap(err, "url.ParseQuery")
	}

	for k, v := range values {
		if len(v) != 1 {
			// TODO LOG ERROR
			continue
		}

		switch k {
		case "token": // []string{"0IGHdWHt6WAAKMVjOwbK2GkM"}
			command.Token = v[0]
		case "user_id": // []string{"U3LH3DZPS"}
			command.GiverID = v[0]
		case "user_name": // []string{"jakubdal"}
			command.GiverName = formating.NormalizeUsername(v[0])
		case "command":
			command.Command = strings.Replace(v[0], "/", "", -1)
		case "text": // []string{"mgregor 2"}
			args := strings.Split(v[0], " ")
			switch len(args) {
			case 1:
				command.ReceiverName = formating.NormalizeUsername(args[0])
				command.Amount = 1
			case 2:
				command.ReceiverName = formating.NormalizeUsername(args[0])
				amount, err := strconv.Atoi(args[1])
				if err != nil {
					// TODO ERROR - INVALID SECOND ARG
					return command, errors.Wrap(err, "strconv.Atoi")
				}
				command.Amount = amount
			default:
				// TODO - RETURN INVALID ARGUMENT COUNT
				return command, fmt.Errorf("invalid argument count: %v", len(args))
			}
		case "response_url": // []string{"https://hooks.slack.com/commands/T3MAQKYPP/467296948391/9TWsM5FhwhHEDPv06bk4UbdM"}
			command.ResponseURL = v[0]
		}
	}

	return command, nil
}
