# I'm Proud - team spirit recognition bot for Slack

With I'm Proud, recognizing amazing behavior of your team is simpler than ever before!

## How it works?

Each user has 5 pride points per day (5 plus points, and 5 minus points) to give to other workspace members (adding points to self is not allowed). All actions are based in slash commands.

### Change user's score

```
/improud <username> <pride_change>
```

For example `/improud @jakubdal 2` would increase @jakubdal's pride by 2 points, accompanied by an ImProud bot's post:

![Pride change](doc/changepride.png)

### Leaderboards

```
/pridemonth
/pridetotal
```

You can see leadersboards for given month with `/pridemonth` or all-time score with `/pridetotal` command:

![Pride leaderboard](doc/totalpride.png)

## Installation

This application is self-hosted, which means you need a server on which to host the app, and you need to set it up in Slack yourself

### Env

- `LISTEN` - host:port on which the app listens, defaults to `8080`
- `LOCATION` - timezone to determine when the day changes, and available points should be reset, defaults to `CET`
- `SLACK_TOKEN` - used to get information about existing users, does not have a default
- `GKE` - set to `true`, if you want to use GCP SQL from GKE, defaults to `false`
- `POSTGRES_HOST` - host of your PostgreSQL instance, defaults to `localhost`
- `POSTGRES_PORT` - port of your PostgreSQL instance, defaults to `5432`
- `POSTGRES_DATABASE` - database name to be used to I'm Proud, defaults to `improud`
- `POSTGRES_USER` - user as which I'm Proud should access the database, defaults to `postgres`
- `POSTGRES_PASSWORD` - password used to authenticate as `POSTGRES_USER`, defaults to `postgres`

### Setup database

Insert `sql/schema.sql` to database you're going to use for your I'm Proud instance.

### Run the app

Use either the docker image, or run `go run *.go` in order to run the application. It's prepared to run on GKE with the PostgreSQL cloud instance.

### Setup Slack

Setup slash commands as described above, and allow I'm Proud to read user informations.

## Known Issues

- New users are only added to workspace on bot restart.
- I forgot how exactly to setup the Slack part of the bot, so that description is a bit lousy.
