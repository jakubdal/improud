FROM golang:1.10.3 as builder

COPY . /go/src/gitlab.com/jakubdal/improud/
WORKDIR /go/src/gitlab.com/jakubdal/improud/
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o improud .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/jakubdal/improud .
CMD ["./improud"]
